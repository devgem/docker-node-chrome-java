# devgem/node-chrome-java

[![node](https://img.shields.io/badge/node-8.15.1-brightgreen.svg)](https://nodejs.org/)
 [![chrome](https://img.shields.io/badge/chrome-72.0.3626.121-brightgreen.svg)](https://www.google.com/chrome/)
 [![java](https://img.shields.io/badge/java-1.8.0_171-brightgreen.svg)](http://openjdk.java.net/install/)
 [![git](https://img.shields.io/badge/git-2.11.0-brightgreen.svg)](https://git-scm.com/)
 [![jq](https://img.shields.io/badge/jq-1.6-brightgreen.svg)](https://stedolan.github.io/jq/)
 [![jq](https://img.shields.io/badge/chromedriver-2.46.628388-brightgreen.svg)](http://chromedriver.chromium.org/)

[![Docker Stars](https://img.shields.io/docker/stars/devgem/node-chrome-java.svg)](https://hub.docker.com/r/devgem/node-chrome-java 'DockerHub')
 [![Docker Pulls](https://img.shields.io/docker/pulls/devgem/node-chrome-java.svg)](https://hub.docker.com/r/devgem/node-chrome-java 'DockerHub')
 [![Docker Layers](https://images.microbadger.com/badges/image/devgem/node-chrome-java.svg)](https://microbadger.com/images/devgem/node-chrome-java)
 [![Docker Version](https://images.microbadger.com/badges/version/devgem/node-chrome-java.svg)](https://hub.docker.com/r/devgem/node-chrome-java)

## Overview

This docker image contains:

* node
* chrome
* java
* envsubst
* git
* jq
* chromedriver
* xfvb

I use it in a [`Gitlab Runner`](https://docs.gitlab.com/runner/) to build, test and document [`Angular`](https://angular.io/) projects created with [`Angular CLI`](https://cli.angular.io/) and create [`Lighthouse`](https://developers.google.com/web/tools/lighthouse/) reports.

To build the image:

```shell
docker build -t devgem/node-chrome-java:VERSION .
```

## Versions

Pre-built images are available on Docker Hub:

| Image | Node | Chrome | Java | Git | Jq | ChromeDriver | Remarks |
|-----|-----|-----|-----|-----|-----|-----|-----|
| [`devgem/node-chrome-java:1.4`](https://hub.docker.com/r/devgem/node-chrome-java/tags/) | 8.15.1 | 72.0.3626.121 | 1.8.0_171 |  2.11.0 | 1.6 | 2.46.628388 | Added Xfvb |
| [`devgem/node-chrome-java:1.3`](https://hub.docker.com/r/devgem/node-chrome-java/tags/) | 8.15.1 | 72.0.3626.121 | 1.8.0_171 |  2.11.0 | 1.6 | 2.46.628388 | |
| [`devgem/node-chrome-java:1.2`](https://hub.docker.com/r/devgem/node-chrome-java/tags/) | 8.15.1 | 72.0.3626.121 | 1.8.0_171 |  2.11.0 | 1.6 | | |
| [`devgem/node-chrome-java:1.1`](https://hub.docker.com/r/devgem/node-chrome-java/tags/) | 8.11.1 | 65.0.3325.181 | 1.8.0_162 |  2.1.4 | | | |
| [`devgem/node-chrome-java:1.0`](https://hub.docker.com/r/devgem/node-chrome-java/tags/) | 8.9.4 | 64.0.3282.186 | 1.8.0_131 |  2.1.4 | | | |
